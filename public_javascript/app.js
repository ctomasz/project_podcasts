var Messages = function(){
    var $box = $('#messages'),
        _class = 'alert';
    var showMessage = function(type, content, flash){
        flash = flash || false;
        type = typeof type === 'undefined' ? 'info' : type;
        content = typeof content !== 'undefined' ? content : '';

        var _class = 'alert-' + type;
        $box.html(content).addClass(_class).show(500);
        if( flash ){
            $box.delay(2500).slideUp();
        }
    }

    function clearBox()
    {
        $box.className = _class;
        $box.hide();
    }
    $box.hide();
    return {
        show: showMessage,
        clear: clearBox
    }
};
var spinIcon = function($el){
    var _content = $el.html(),
        status = $el.data('spin') || 0,
        start = function(){
            if( status === 0 ) {
                $el.html( '<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + _content );
                $el.data('spin',1);
            }
        },
        stop = function(){
            $el.html( _content);
            $el.data('spin',0);
        };
    return {
        start: start,
        stop: stop
    }
};

var makeAjaxCall = function(url,data, callbackOK, callbackError){
    $.ajax({
        url: url,
        data: data,
        method: "POST",
        success: function(data){
            if( 'status' in data ) {
                if( data.status == 'success' && typeof callbackOK === 'function') callbackOK(data.content);
                if( data.status == 'error' && typeof callbackError === 'function') callbackError(data.content);
            }
        },
        error: function(a,b,c){
           var _res = 'Some error. Please try again.';
            _res = (typeof a.responseJSON !== 'undefined' && typeof a.responseJSON.content !== 'undefined') ? a.responseJSON.content : _res;
            if( typeof callbackError === 'function') callbackError(_res);
        }

    });
};

var forms = function(){
    var $this = $(this),
        status = 0,
        $form1 = $('#mainForm'),
        $form2 = $('#form2');

    if( status === 0 ) {
        $this.html( $form1.html() );
    }

    $this.on('click','#btn_checkurl', function(e){
        e.preventDefault();
        var $inputUrl = $("input[name='newurl']");

        $inputUrl.parent().removeClass('has-error');
        var spin = spinIcon($(this));
            spin.start();
        if( $inputUrl.val().length > 3) {
            messages.clear();
            makeAjaxCall('/check-url', {url: $inputUrl.val()}, function(content){
                var _templ = Mustache.render( $form2.html(), content);
                $this.slideUp(700, function(){
                    $this.html( _templ).slideDown();
                });
                spin.stop();
            }, function(content){
                messages.show('danger', content);
                spin.stop();
            });
        } else {
            $inputUrl.parent().addClass('has-error');
            spin.stop();
        }
    });

    $this.on('click', '#reset', function(e){
        e.preventDefault();
        messages.clear();
        $this.html( $form1.html() );
    });

    $this.on('submit', '#saveForm', function(e){
        e.preventDefault();
        messages.clear();
        var spin = spinIcon($(this).find('#btnSave') );
        spin.start();
        var _data = $this.find('form').serialize();
        makeAjaxCall('/new-podcast', _data, function(content){
            podcasts.addBox( content.box );
            $this.html( $form1.html() );
            messages.show('success', content.message, true);
            spin.stop();
        }, function(content){
            messages.show('danger', content);
            spin.stop();
        });
    });
};

var Podcasts = function(){
    var $this = $('#itemsList');

    function addBox(content){
        $this.prepend( content);
    }

    $this.on('click', '.hideItem', function(e){
        e.preventDefault();
        var $btn = $(this),
            id = $btn.data('item'),
            spin = spinIcon( $btn );
        console.log('hide item id=' + id);

        spin.start();
        makeAjaxCall('un-likeit', {id: id}, function(){
            $btn.parents('.row').slideUp();
            spin.stop();
        }, function(content){
            messages.show('warning', content, true);
            spin.stop();
        });
    });

    $this.on('click', '.likeIt', function(e){
        e.preventDefault();
        var $btn = $(this),
            id = $btn.data('item'),
            spin = spinIcon( $btn);
        spin.start();
        makeAjaxCall('/like-it', {id: id}, function(){
            $btn.toggleClass('redin');
            spin.stop();
        }, function(content){
            messages.show('warning', content, true);
            spin.stop();
        });
    });

    $this.on('click', '.pinItem', function(e){
        e.preventDefault();
        var $btn = $(this),
            id = $btn.data('item'),
            spin = spinIcon( $btn );
        spin.start();
        makeAjaxCall('/interesting', {id: id}, function(){
            $btn.addClass('redin');
            spin.stop();
        }, function( content ){
            if( content.length > 0 ) messages.show('warning', content, true);
            spin.stop();
        });
    });


    return {
        addBox: addBox
    };

};

// init
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var messages = new Messages();
var podcasts = new Podcasts();

$.fn.extend({forms: forms})
$('#forms').forms();


