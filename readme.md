# Portfolio - Podcasts Folder

> Repository contains few elements of "Podcasts forlder" project.  - http://podcasts.project.ctomweb.com

#### Service uses the following technologies: 
* backendu
    * framework Laravel5
    * library [guzzlehttp]
    * component Symfony [DomCrawler]
* frontend
    * jQuery / Ajax
    * [Mustache.js]
    * Bootstrap 3

[DomCrawler]:http://symfony.com/doc/current/components/dom_crawler.html
[guzzlehttp]:http://guzzle.readthedocs.org/en/latest/
[Mustache.js]:https://github.com/janl/mustache.js
