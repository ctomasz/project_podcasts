<?php
namespace App\Repositories\Podcast;

use App\Models\Podcast;
use DB;
use Exception;

class EloquentPodcast implements PodcastInterface {
    /**
     * @var Model
     */
    private $model;

    public function __construct(Podcast $model)
    {
        $this->model = $model;
    }

    public function getItemById($id)
    {
        try{
            $item = $this->model->where('id', $id)->firstOrFail();
        } catch( Exception $ex){
            return null;
        }
        return (object)$item->toArray();
    }

    public function getAllPodcastsPerPageWithUser($page, $perPage, $userId)
    {
        $count = $this->model->with(['user' => function($q) use ($userId){
            $q->where('id', $userId);
        }])->count();

        $list = $this->model->with(['user' => function($q) use ($userId){
            $q->where('id', $userId);
        }])->orderBy('created_at','DESC')->get();

        $list->each(function($row){
            $row['favorite'] = $row->user->count() > 0 ? true : false;
        });


        return (object)[
            'items' => (object)$list->toArray(),
            'count' => $count,
            'perPage' => $perPage,
            'page' => $page
        ];
    }

    public function getAllPodcastsPerPage($page, $perPage)
    {
        $count = $this->model->count();
        $list = $this->model->skip( ($page-1) * $perPage)->take($perPage)->get();

        return (object)[
            'items' => (object)$list->toArray(),
            'count' => $count,
            'perPage' => $perPage,
            'page' => $page
        ];
    }

    public function getMyPodcastsPerPage($userId, $page, $perPage)
    {
        $count = $this->model->whereHas('user', function($q) use ($userId){
            $q->where('id', $userId);
        })->count();

        $list = $this->model->whereHas('user', function($q) use ($userId){
                $q->where('id',$userId);
            })->with('user')
            ->orderBy('created_at','DESC')
            ->skip( ($page-1) * $perPage)->take($perPage)
            ->get();

        $list->each(function($row){
            $row['likeit'] = $row->user->first()->pivot->likeit;
            $row['interest'] = $row->user->count();
        });

        return (object)[
            'items' => (object)$list->toArray(),
            'count' => $count,
            'perPage' => $perPage,
            'page' => $page
        ];
    }

    public function addNew($podcast)
    {
        try{

            $item = $this->model->create($podcast);
        } catch( Exception $ex) {
            return false;
        }

        return (object)$item->toArray();
    }

    public function pinPodcast($userId, $podcastId)
    {
        try {
            $podcast = $this->model->where('id', $podcastId)->firstOrFail();
            $podcast->user()->attach([$userId]);
        } catch (Exception $ex){
            return false;
        }
        return true;

    }

    public function toggleLikePodcast($userId, $podcastId)
    {
        if( is_null( $podcastId)) return false;
        $st = DB::update('UPDATE podcast_user SET likeit = CASE WHEN likeit = 1 THEN 0 ELSE 1 END WHERE user_id = ? AND podcast_id = ?', [$userId, $podcastId]);

        return $st > 0 ? true : false;
    }

    public function removeFromCollection($userId, $podcastId)
    {
        try{
            $podcast = $this->model->where('id', $podcastId)->firstOrFail();
            $podcast->user()->detach([$userId]);
        } catch( Exception $ex) {
            return false;
        }
        return true;

    }
}