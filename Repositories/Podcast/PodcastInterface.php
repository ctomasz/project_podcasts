<?php
namespace App\Repositories\Podcast;


interface PodcastInterface {
    /**
     * @param $id
     * @return null|object
     */
    public function getItemById($id);

    /**
     * @param int $page
     * @param int $perPage
     * @param int $userId
     * @return object
     */
    public function getAllPodcastsPerPageWithUser($page, $perPage, $userId);

    /**
     * @param int$page
     * @param int $perPage
     * @return object
     */
    public function getAllPodcastsPerPage($page, $perPage);

    /**
     * @param int $userId
     * @param int $page
     * @param int $perPage
     * @return object
     */
    public function getMyPodcastsPerPage($userId, $page, $perPage);

    /**
     * @param array $podcast
     * @return false|object
     */
    public function addNew($podcast);

    /**
     * @param int $userId
     * @param int $podcastId
     * @return boolean
     */
    public function pinPodcast($userId, $podcastId);

    /**
     * @param int $userId
     * @param int $podcastId
     * @return boolean
     */
    public function toggleLikePodcast($userId, $podcastId);

    /**
     * @param int $userId
     * @param int $podcastId
     * @return boolean
     */
    public function removeFromCollection($userId, $podcastId);
}