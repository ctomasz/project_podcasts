<?php

namespace App\Http\Controllers;

use App\Repositories\Podcast\PodcastInterface;
use App\Utilities\PodcastEntityAdapter;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Crypt;

class PageController extends Controller
{


    public function getSignIn()
    {
        if( auth()->check() ) return redirect()->route('home');

        return view('signin');
    }

    public function postSignIn(Request $request)
    {

        if( auth()->attempt(['email' => $request->get('login',null), 'password' => $request->get('password','null')], true) ){
            return redirect()->route('my')->withMessage('Login success!');
        }

        $request->session()->flash('errorMessage', trans('messages.login-error'));
        return redirect()->back()
                ->withInput(['login']);

    }

    public function getLogout()
    {
        auth()->logout();
        return redirect()->route('signin');
    }

    public function getSignUp()
    {
        if( auth()->check() ) return redirect()->route('home');
        return redirect()->route('signin');
    }

    public function allPodcasts(PodcastInterface $podcast, Request $request)
    {
        $page = $request->get('page',1);
        $perPage = 25;
        if( auth()->check() ){
            $list = $podcast->getAllPodcastsPerPageWithUser($page, $perPage, auth()->user()->id);
        } else {
            $list = $podcast->getAllPodcastsPerPage($page, $perPage);
        }
        $paginator = new Paginator($list->items, $perPage, $page);

        return view('allPodcasts')
                ->with('items', $list->items)
                ->with('menuAll', true)
                ->with('pagination',$paginator->render());
    }

    public function myPodcast(PodcastInterface $podcast, Request $request)
    {
        if(  !auth()->check() ) return redirect()->route('signin');
        $page = $request->get('page', 1);
        $perPage = 25;

        $list = $podcast->getMyPodcastsPerPage(auth()->user()->id, $page, $perPage);
        $paginator = new Paginator($list->items, $perPage, $page);
        $items = ( new PodcastEntityAdapter($list->items, auth()->user()->id))->get();

        return view('myPodcasts')
            ->with('items', $items)
            ->with('menuMy', true)
            ->with('pagination',$paginator->render());
    }

    public function gotoWWW(PodcastInterface $podcast, $id)
    {
        $id = Crypt::decrypt($id);

        try{
            if(null == $item = $podcast->getItemById($id)) throw new Excelption;
            return redirect($item->www);
        } catch (Exception $ex){
            abort(404);
        }

    }

    public function download(PodcastInterface $podcast, $id)
    {
        $id = Crypt::decrypt($id);

        try{
            if(null == $item = $podcast->getItemById($id)) throw new Excelption;
            return redirect($item->audio);
        } catch (Exception $ex){
            abort(404);
        }
    }

}
