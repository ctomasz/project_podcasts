<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessPage;
use App\Repositories\Podcast\EloquentPodcast;
use App\Repositories\Podcast\PodcastInterface;
use Carbon\Carbon;
use ctomasz\Crawler\ConnectUrlException;
use ctomasz\Crawler\Url;
use ctomasz\Crawler\ValidateUrlException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;

class PodcastController extends Controller
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var EloquentPodcast
     */
    private $podcast;

    public function __construct(Request $request, PodcastInterface $podcast)
    {
        $this->request = $request;
        $this->podcast = $podcast;
    }

    public function checkUrl()
    {

        $url = $this->request->get('url', null);
        if( strpos($url, 'http') === true ) {
            $url = 'http://'.$url;
        }

        $urlService = new Url();
        try {
            $urlService->check($url);

            $json = [
                'status' => 'success',
                'content' => [
                    'url' => $url,
                    'title' => '',
                    'desc' => '',
                    'audio' => false,
                    'detail' => [
                        'url' => $url,
                        'type' => 'audio',
                        'length' => 0
                    ]
                ]
            ];

            $typePrefix = explode('/', $urlService->getContentType())[0];
            switch($typePrefix){
                case 'text': {
                    $page = new ProcessPage(  $urlService->getContent() );
                    $json['content']['title'] = $page->getTitle();
                    $json['content']['desc'] = $page->getDesc();
                    $json['content']['img'] = $page->getImg();
                    break;
                }
                case 'audio': {
                    $json['content']['audio'] = true;
                    $json['content']['detail']['length'] = number_format(($urlService->getLength() / 1000000),2);
                    break;
                }
                default: {
                    throw new ValidateUrlException();
                }
            }

        } catch ( ValidateUrlException $ex) {
            return response()->json([
                'status' => 'error',
                'content' => trans('messages.url-validation')
            ]);
        } catch ( ConnectUrlException $ex) {
            return response()->json([
                'status' => 'error',
                'content' => trans('messages.url-error')
            ]);
        }

        $this->request->session()->put('newUrl', $json);

        return response()->json($json);

    }

    public function saveNewPodcast(Request $request)
    {
        $data = $request->all();

        try{
            $old = $request->session()->get('newUrl', ['content' => []]);
            $data = array_merge( $old['content'], $data);

            $podcast = $this->prepareData($data);

            if( false === $item = $this->podcast->addNew($podcast)) throw new Exception;
        } catch ( Exception $ex) { dd($ex);
            return response()->json([
                'status' => 'error',
                'content' => trans('messages.error')
            ]);
        }


        $item->favorite = false;
        $block = view('partials.boxBlock')->with('item', $item)->render();

        return response()->json([
           'status' => 'success',
            'content' => [
                'box' => $block,
                'message' => 'Thank you! New podcast has added.',
            ]
        ]);
    }

    public function userInterest()
    {
        try{
            if( !auth()->check() ) throw new Exception( trans('messages.error') );

            if( false === $this->podcast->pinPodcast(auth()->user()->id, $this->request->get('id', null)) ){
                throw new Exception('');
            }
        } catch (Exception $ex) {
            return response()->json([
                'status' => 'error',
                'content' => $ex->getMessage()
            ]);
        }

        return response()->json([
            'status' => 'success',
            'content' => ''
        ]);
    }

    public function userUnInterest()
    {
        try{
            if( !auth()->check() ) throw new Exception();

            if( false === $this->podcast->removeFromCollection(auth()->user()->id, $this->request->get('id', null)) ){
                throw new Exception();
            }
        } catch (Exception $ex) {
            return response()->json([
                'status' => 'error',
                'content' => trans('messages.error')
            ]);
        }

        return response()->json([
            'status' => 'success',
            'content' => ''
        ]);
    }


    public function userLikeIt()
    {
        try{
            if( !auth()->check() ) throw new Exception();

            if( false === $this->podcast->toggleLikePodcast(auth()->user()->id, $this->request->get('id', null)) ){
                throw new Exception();
            }
        } catch (Exception $ex) {
            return response()->json([
               'status' => 'error',
                'content' => trans('messages.error')
            ]);
        }

        return response()->json([
            'status' => 'success',
            'content' => ''
        ]);

    }

    protected function prepareData($data)
    {
        if( $data['audio'] === true) {
            $www = strlen($data['copyright']) > 0 ? $data['copyright'] : null;
            $audio = $data['detail']['url'];
        } else {
            $www = strlen($data['audio']) > 0 ? $data['audio'] : $data['url'];
            $audio = strlen($data['audio']) > 0 ? $data['audio'] : null;
        }

        return [
            'title' => strlen($data['title']) > 0 ? $data['title'] : 'Podcast - '.Carbon::now(),
            'www' => $www,
            'audio' => $audio,
            'img' => isset($data['img']) && strlen($data['img']) > 0 ? $data['img'] : null,
            'desc' => $data['desc'],
            'copyright' => isset($data['copyright']) ? $data['copyright'] : null,
            'add_by' => auth()->user()->id
        ];
    }
}
